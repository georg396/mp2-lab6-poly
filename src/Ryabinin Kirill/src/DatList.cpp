#include "DatList.h"

TDatList::TDatList()
{
    pFirst = pLast = pStop = NULL;
    CurrPos = -1;
    ListLen = 0;
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
    if (pLink->pValue != NULL)
        delete pLink->pValue;
    delete pLink;
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
    PTDatLink temp = NULL;

    switch (mode)
    {
    case FIRST:
        temp = pFirst; break;
    case CURRENT:
        temp = pCurrLink; break;
    case LAST:
        temp = pLast; break;
    }

    if (temp != NULL)
        return temp->GetDatValue();
    else
        return NULL;
}

int TDatList::SetCurrentPos(int pos)
{
    if (pos >= 0 && pos <= ListLen) {
        Reset();
        for (int i = 0; i < pos; i++)
            if (GoNext() == ERROR)
                return ERROR;
    }
    else
        return ERROR;

    return OK;
}

int TDatList::GetCurrentPos(void) const
{
    return CurrPos;
}

int TDatList::Reset(void)
{
    pPrevLink = pStop;

    if (!IsEmpty()) {
        pCurrLink = pFirst;
        CurrPos = 0;
        return OK;
    }
    else {
        pCurrLink = pStop;
        CurrPos = -1;
        return ERROR;
    }
}

bool TDatList::IsListEnded(void) const
{
    return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
    if (!IsListEnded()) {
        pPrevLink = pCurrLink;
        pCurrLink = pCurrLink->GetNextDatLink();
        CurrPos++;
    }
    else
        return ERROR;

    return OK;
}

void TDatList::InsFirst(PTDatValue pVal)
{
    PTDatLink temp = GetLink(pVal, pFirst);

    if (temp != NULL) {
        pFirst = temp;
        ListLen++;

        if (ListLen == 1) {
            pLast = pFirst;
            Reset();
        }
        else if (CurrPos == 0)
            pCurrLink = temp;
        else
            CurrPos++;
    }
}

void TDatList::InsLast(PTDatValue pVal)
{
    PTDatLink temp = GetLink(pVal, pStop);

    if (temp != NULL) {
        if (pLast != NULL)
            pLast->SetNextLink(temp);
        pLast = temp;
        ListLen++;

        if (ListLen == 1) {
            pFirst = pLast;
            Reset();
        }

        if (IsListEnded() == true)
            pCurrLink = pFirst;
    }
}

void TDatList::InsCurrent(PTDatValue pVal)
{
    if ((pCurrLink == pFirst) || (IsEmpty() == true))
        InsFirst(pVal);
    else if (IsListEnded() == true)
        InsLast(pVal);
    else {
        PTDatLink temp = GetLink(pVal, pCurrLink);

        if (temp != NULL) {
            pPrevLink->SetNextLink(temp);
            ListLen++;
            pCurrLink = temp;
        }
    }
}

void TDatList::DelFirst(void)
{
    if (IsEmpty() != true) {
        PTDatLink temp = pFirst;

        pFirst = pFirst->GetNextDatLink();
        DelLink(temp);
        ListLen--;

        if (IsEmpty() == true) {
            pLast = pStop;
            Reset();
        }
        else if (CurrPos == 0)
            pCurrLink = pFirst;
        else
            CurrPos--;
    }
}

void TDatList::DelCurrent(void)
{
    if (pCurrLink != NULL) {
        if ((pCurrLink == pFirst) || (IsEmpty() == true))
            DelFirst();
        else {
            PTDatLink temp = pCurrLink;

            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            DelLink(temp);
            ListLen--;

            if (pCurrLink == pLast) {
                pLast = pPrevLink;
                pCurrLink = pStop;
            }
        }
    }
}

void TDatList::DelList(void)
{
    while (IsEmpty() != true)
        DelFirst();
    CurrPos = -1;
    ListLen = 0;
    pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop = NULL;
}