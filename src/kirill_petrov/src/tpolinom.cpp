// Copyright 2016 Petrov Kirill

#include "tpolinom.h"

// ����������� � �������� ���������� ������� � � ��������
TPolinom:: TPolinom(int monoms[][2], int km)
{
	PTMonom Mon=new TMonom(0,-1);
	pHead->SetDatValue(Mon);
	for (int i = 0; i < km; i++)
	{
		Mon = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(Mon);
	}	
}

// ����������� �����������
TPolinom:: TPolinom(TPolinom &q)
{
	PTMonom Mon = new TMonom(0, -1);
	pHead->SetDatValue(Mon);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		Mon = q.GetMonom();
		InsLast(Mon->GetCopy());
	}
}

// �������� ���� ���������
TPolinom TPolinom:: operator+(TPolinom &q)
{
	TPolinom res;

	// ���������� ��� ������, �������� ����� �������
	q.Reset();
	Reset();

	// �������������� ���������� � �������
	const TMonom nul(0, -1);	// ������� �������
	TMonom mon;					// ����� ��� �������� ��������
	TMonom mon_q;				// ����� ��� �������� ��������
	PTMonom tmp;				// ��������� �� �������, � ������� ����� ��������� ����� ������

	// ����� ���� ���� ��� ������ �� ������ �� �����
	while ( !IsListEnded() || !q.IsListEnded() )
	{
		// ���� ������ � ������� ����� �� �������, ����������� ���������� ����� 
		mon = IsListEnded() ? nul: *GetMonom();
		mon_q = q.IsListEnded() ? nul: *q.GetMonom();

		// ���� �� ���� ���� �� ���� �������� ������� ��� ������ ��� ���� �� ������� ��������
		// ������ ���� ����������� � ������� ������ ����� � ��������� ������ ������
		// ���� ����� ��� 5x^3 � 4x^2+2x+3 ����� ��� ��������� � ����� ������ �������� 2x,3
		if (mon == nul)
		{
			tmp = new TMonom( mon_q.GetCoeff(), mon_q.GetIndex() );
			q.GoNext();
		}
		else if (mon_q == nul)
		{
			tmp = new TMonom( mon.GetCoeff(), mon.GetIndex() );
			GoNext();
		}

		// ���� ������ ��������� � ��������� �������,
		// �� ����� �������� ������� ������ � �������
		else if (mon < mon_q)
		{
			tmp = new TMonom( mon.GetCoeff(), mon.GetIndex() );
			GoNext();
		}
		else if (mon_q < mon)
		{
			tmp = new TMonom( mon_q.GetCoeff(), mon_q.GetIndex() );
			q.GoNext();
		}

		// ���� ����� ������, �� ������ �����, � ������������ �����
		else if ( mon.GetIndex() == mon_q.GetIndex() )
		{
			tmp = new TMonom( mon.GetCoeff() + mon_q.GetCoeff(), mon.GetIndex() );
			GoNext();
			q.GoNext();
		}

		// � ������ ������������� ��������
		else
			break;

		// ����� �������� � ������� ������ !��������� ������
		if ( tmp->GetCoeff() )
			res.InsLast(tmp);
	}
	return res;
}


// ���������� ���� ���������.
TPolinom & TPolinom:: operator=(TPolinom &q)
{
	// ����� ������������� ������� ������ �������
	DelList();
	// ������ ������, �������� � ���������
	// � ��������� �������� ��� �������

	// �������������� �������� �� ������� � 
	// ���� �� ��������� �� ���� ��
	if ( (&q!=NULL) && (&q != this) )
	{
		PTMonom Mon = new TMonom(0, -1);
		pHead->SetDatValue(Mon);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			Mon = q.GetMonom();
			InsLast(Mon->GetCopy());
		}
	}
	// ���������� ����
	return *this;
}

/*
	�����, ����������� ��������� ����������� �� ����������� 
	���������� x,y ��� z. ������������, ���� ��������.
	�������� 4 ����� �� ���� �����, ��� ��������.
	���������� ����������� ��������� ��������� �� ������� ax^n=anx^(n-1)
*/
TPolinom TPolinom:: Derivative(Variables v)
{
	TPolinom result;
	int Index;

	// ��������� �������, ����������� ���������� ����������� ����������
	// �������� 800 �� x �������� 8
	auto IndexVar = [v](unsigned Index)
	{
		unsigned result;
		switch (v)
		{
		case z: result = Index % 10; break;
		case y:  result = Index % 100 / 10; break;
		case x:  result = Index / 100; break;
		}
		return result;
		
	};
	
	// ������ ����� ������ � ��������� ����� �������
	PTMonom Mon = new TMonom(0, -1);
	for (Reset(); !IsListEnded(); GoNext())
	{
		// �� ���� ����� ������
		Index = GetMonom()->GetIndex();
		if ( IndexVar( Index ) )
		{
			Mon = new TMonom( GetMonom()->GetCoeff() * IndexVar( Index), Index-v );
			result.InsLast(Mon);
		}
	}
	return result;
}

 /* 
	�������� ��������� ���� ���������. ������� � �����
	��������� � ������. �������� ��������� �������� �� �������
	������������ ������� �������� (GoNext, Reset). ������� 1 ����
	� ������ �������.
*/
bool TPolinom:: operator==(TPolinom &q)
{
	bool result = false;
	if (q.GetListLength() == GetListLength())
	{
		result = true;
		for (q.Reset(), Reset(); !q.IsListEnded(); q.GoNext(), GoNext())
			if (*(PTMonom)q.GetDatValue() != *(PTMonom)GetDatValue())
			{
				result = false;
				break;
			}
	}
	return result;
}

ostream & operator<<(ostream & os, TPolinom & q)
{
	// ��������� �������, ������� ��������� ������ � ������
	auto CovertIndex = [](int index)
	{
		int tmp;
		string result;
		if (tmp = index / 100)
			result += tmp > 1 ? "x^" + lexical_cast <string> (tmp) : "x";
		if (tmp = index % 100 / 10)
			result += tmp > 1 ? "y^" + lexical_cast <string> (tmp) : "y";
		if (tmp = index % 10)
			result += tmp > 1 ? "z^" + lexical_cast <string> (tmp) : "z";
		return result;
	};
	// ��������� ���� ������. ������� � ����� ���������
	q.Reset();
	os << q.GetMonom()->GetCoeff() << CovertIndex(q.GetMonom()->GetIndex());
	q.GoNext();
	for ( ; !q.IsListEnded(); q.GoNext() )
		os << ( q.GetMonom()->GetCoeff() > 0 ? " +" : " " ) << q.GetMonom()->GetCoeff() << CovertIndex(q.GetMonom()->GetIndex() );
	return os;
}


typedef regex_iterator<string::iterator> inter;

istream & operator>>(istream & os, TPolinom & q)
{
	// ��������������� ����������
	int i = 0, coeff,index;
	string buf;

	// ���� "��������"
	os >> buf;

	try
	{
		PTMonom mon;
		// ��������� ��������� �� ���������� ������� ������
		regex re_input("(([\\+|-]|^)[1-9](\\d+)?x\\^\\dy\\^\\dz\\^\\d)+");
		// �������� ��� ����������� � ������
		regex re_coeff("^\\d+|(\\+|-)((\\d+))");
		// �������� ��� ������� �� ^
		regex re_index("(\\^\\d)");

		// ���� �� ����������� ������� ����������� ���������, ����������� ����������
		if (!regex_match(buf, re_input)) throw exception ("Wrong input format");
		

		// ��� ���������, �� ������ ��������� ���������� ���������
		inter it_index(buf.begin(), buf.end(), re_index);
		inter it_coeff(buf.begin(), buf.end(), re_coeff);

		// �������� ��� ���������� ��������� � ������ �� ���� ������ �����
		for (; it_coeff != inter(); ++it_coeff, ++i)
		{
			// �������� ������������
			coeff = lexical_cast <int>(it_coeff->str());

			// ������� ����������� ������ ������ x*100+y*10+z*1
			index = lexical_cast <int>(it_index++->str()[1]) * 100 +  //x
				lexical_cast <int>(it_index++->str()[1]) * 10 +       //y
				lexical_cast <int>(it_index++->str()[1]);             //z
			
			// ������ ����� � ������� � ����� ������
			mon = new TMonom(coeff, index);
			q.InsLast(mon);
		}

	}
	// ���������� �� ������ ������ ������
	catch (const regex_error& e)
	{
		cout << e.what() << endl;
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "Unknown error" << endl;
	}
	return os;
}
