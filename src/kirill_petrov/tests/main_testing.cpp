// Copyright 2016 Petrov Kirill

//#define TEST

#ifdef TEST

#include "gtest/gtest.h"

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
#else

// SAMPLE

#include "tpolinom.h"
#include <locale>

int main()
{
	setlocale(LC_ALL, "rus");
	TPolinom P1,P2;
	char YesNo;
	cout << "�������� ��������� ��� ������������, �� ������������ �������, �������� ������ ����������:" << endl;
	cout << "��������� ��� ����������, ���������� ������ ������� � ���������� x^,y^,z^ " << endl;
	cout <<	"��������� ����������� ��������� ������������ ����� ������ �������" << endl;
	cout << "��������: 15x^0y^0z^0+5x^2y^1z^3+10x^4y^3z^2-20x^5y^0z^0+20x^7y^0z^2+10x^9y^9z^9" << endl;

	do
	{
		cout << "������� ������ ������� " << endl;
		cin >> P1;
		cout << "���������� �� ����� ������� ��������� � ����� ��������? Y/N" << endl;
		cout << P1 << endl;
		cin >> YesNo;
	} while (YesNo == 'N');

	do
	{
		cout << "������� ������ ������� " << endl;
		cin >> P2;
		cout << "���������� �� ����� ������� ��������� � ����� ��������? Y/N" << endl;
		cout << P2 << endl;
		cin >> YesNo;
	} while (YesNo == 'N');

	cout << "����� ���� ��������� �����: " << endl;
	cout << (P1+P2) << endl;
	cout << endl;
	cout << "����������� ������� �������� �� x �����: " << endl;
	cout << P1.Derivative(x) << endl;
	cout << "����������� ������� �������� �� y �����: " << endl;
	cout << P1.Derivative(y) << endl;
	cout << "����������� ������� �������� �� z �����: " << endl;
	cout << P1.Derivative(z) << endl;
	cout << endl;
	cout << "����������� ������� �������� �� x �����: " << endl;
	cout << P2.Derivative(x) << endl;
	cout << "����������� ������� �������� �� y �����: " << endl;
	cout << P2.Derivative(y) << endl;
	cout << "����������� ������� �������� �� z �����: " << endl;
	cout << P2.Derivative(z) << endl;
}
#endif