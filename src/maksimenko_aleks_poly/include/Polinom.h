#pragma once
#include "HeadRing.h"
#include "Monom.h"

#include <string>
#include <iostream>
#include <exception>

using namespace std;

enum variables { x = 100, y = 10, z = 1 };

class TPolinom : public THeadRing {
public:
    TPolinom(int monoms[][2] = NULL, int km = 0);                // ����������� �������� �� ������� ������������-������
    TPolinom(TPolinom &q);                                       // ����������� �����������
    PTMonom  getMonom() { return (PTMonom)GetDatValue(); }
    TPolinom operator+(TPolinom &q);                             // �������� ���������
    TPolinom operator=(TPolinom &q);                             // ������������
    double calc(double x = 0.0, double y = 0.0, double z = 0.0); // ���������� �������� �������� � ��������� ���������� ����������
    friend istream & operator>>(istream & os, TPolinom & q);     // ���� ��������
    friend ostream & operator<<(ostream & os, TPolinom & q);     // �����
};
