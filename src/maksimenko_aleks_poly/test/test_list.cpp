#include "gtest/gtest.h"
#include "DatList.h"

typedef struct ValueInt :TDatValue
{
    int Value;
    ValueInt() { Value = 0; }
    virtual TDatValue * GetCopy() { TDatValue *temp = new ValueInt; return temp; }
} *PValueInt;

class TestingDataList : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        Val = new ValueInt();
        Val->Value = 100;

        ArrVal = new PValueInt[N];
        for (int i = 0; i < N; i++)
        {
            ArrVal[i] = new ValueInt();
            ArrVal[i]->Value = i;
        }
    }
    virtual void TearDown()
    {
        delete ArrVal;
    }
    const int N = 10;
    PValueInt Val;
    PValueInt *ArrVal;
    TDatList List;
};

TEST_F(TestingDataList, Correct_Size_List) {
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);

    EXPECT_EQ(List.GetListLength(), N);
}

TEST_F(TestingDataList, �an_Delete_Elem_From_Beginnig) {
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    List.DelFirst();

    bool result_left, result_right;
    result_left = (((PValueInt)List.GetDatValue())->Value == (ArrVal[1]->Value));
    result_right = ((List.GetListLength()) == (N - 1));
    EXPECT_EQ(result_left, result_right);
}

TEST_F(TestingDataList, Can_Add_An_Item_To_The_End) {
    List.InsLast(Val);

    PValueInt temp = (PValueInt)List.GetDatValue();
    EXPECT_EQ(Val->Value, temp->Value);
}


TEST_F(TestingDataList, Can_Add_Ten_Elems_To_The_End) {
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);

    for (int i = 0; i < N; i++, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestingDataList, Can_Add_Ten_Elems_To_The_Beginnig) {
    for (int i = 0; i < N; i++)
        List.InsFirst(ArrVal[i]);

    for (int i = N - 1; i > 0; i--, List.GoNext())
        EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[i]->Value);
}

TEST_F(TestingDataList, Can_Delete_Elem_From_The_Current_Pos) {
    for (int i = 0; i < N; i++)
        List.InsLast(ArrVal[i]);
    List.GoNext();
    List.DelCurrent();

    EXPECT_EQ(((PValueInt)List.GetDatValue())->Value, ArrVal[2]->Value);
    EXPECT_EQ(List.GetListLength(), N - 1);
}

