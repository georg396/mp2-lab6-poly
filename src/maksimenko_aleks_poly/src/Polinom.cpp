#include "Polinom.h"
#include "Exception.h"

TPolinom::TPolinom(int monoms[][2], int km) { // ����������� �������� �� ������� ������������-������
    PTMonom elem = new TMonom(0, -1);

    pHead->SetDatValue(elem);

    for (int i = 0; i < km; i++) {
        elem = new TMonom(monoms[i][0], monoms[i][1]);
        InsLast(elem);
    }
}

TPolinom::TPolinom(TPolinom &q) { // ����������� �����������
    PTMonom Mon = new TMonom(0, -1);
    pHead->SetDatValue(Mon);
    for (q.Reset(); !q.IsListEnded(); q.GoNext())
    {
        Mon = q.getMonom();
        InsLast(Mon->GetCopy());
    }
}

TPolinom TPolinom::operator+(TPolinom & q) { // �������� ���������
    TPolinom result;
    PTMonom resultMon = nullptr;
    TMonom mon_fir, mon_sec;
    const TMonom null(0, -1);

    Reset();
    q.Reset();

    while (!IsListEnded() || !q.IsListEnded()) {
        mon_fir = IsListEnded() ? null : *getMonom();
        mon_sec = q.IsListEnded() ? null : *q.getMonom();

        if (mon_fir == null) {
            resultMon = new TMonom(mon_sec.GetCoeff(), mon_sec.GetIndex());
            q.GoNext();
        }
        else if (mon_sec == null) {
            resultMon = new TMonom(mon_fir.GetCoeff(), mon_fir.GetIndex());
            GoNext();
        }

        else if (mon_fir < mon_sec) {
            resultMon = new TMonom(mon_fir.GetCoeff(), mon_fir.GetIndex());
            GoNext();
        }
        else if (mon_sec < mon_fir) {
            resultMon = new TMonom(mon_sec.GetCoeff(), mon_fir.GetIndex());
            q.GoNext();
        }

        else if (mon_fir.GetIndex() == mon_sec.GetIndex()) {
            resultMon = new TMonom(mon_fir.GetCoeff() + mon_sec.GetCoeff(), mon_fir.GetIndex());
            GoNext();
            q.GoNext();
        }

        else
            break;

        if (resultMon->GetCoeff())
            result.InsLast(resultMon);
    }

    return result;
}

TPolinom TPolinom::operator=(TPolinom & q) { // ������������
    DelList();
    if ((&q != nullptr) && (&q != this)) {
        PTMonom Mon = new TMonom(0, -1);
        pHead->SetDatValue(Mon);

        for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
            Mon = q.getMonom();
            InsLast(Mon->GetCopy());
        }
    }

    return *this;
}

double TPolinom::calc(double x, double y, double z) { // ���������� �������� �������� � ��������� ���������� ����������
    double result = 0.0;
    int coeff, index;
    int index_x, index_y, index_z;

    for (Reset(); !IsListEnded(); GoNext()) {
        coeff = getMonom()->GetCoeff();
        index = getMonom()->GetIndex();

        index_x = index / 100;
        index_y = (index % 100) / 10;
        index_z = index % 10;

        result += coeff * pow(x, index_x) * pow(y, index_y) * pow(z, index_z);
    }

    return result;
}

istream & operator>>(istream & os, TPolinom & q) { // ���� ��������
    int coeff = NULL, index = NULL, count = -1, k = -1;
    string end = "end";
    string buf = "";
    PTMonom mon;

    try {
        while (true) {
            os >> buf;
            count++;
            if (count == 4) {
                count = NULL;
                mon = new TMonom(coeff, index);
                q.InsLast(mon);
            }
            if ((count == NULL) && (buf == end)) {
                break;
            }
            else
                if ((count != 0) && (buf == end)) throw  pendingTheInput(); // ����������: ������������ ����_4 - ������������� ���� ��������.
            switch (count) {
            case 0: // ��������� ������������ ������.
                if ((buf.length() == 1) && !(('0' < buf[NULL]) && (buf[NULL] <= '9'))) throw  notEqualTheNumber(); // ����������: ������������ ����_1 - ����������� ������ ����� ������� �����.
                else
                    if (('0' <= buf[NULL]) && (buf[NULL] <= '9')) coeff = buf[NULL] - 48;
                    else
                        if (buf.length() > 1)
                        if ((buf[NULL] == '+') || (buf[NULL] == '-') && (!(('0' < buf[NULL + 1]) && (buf[NULL + 1] <= '9')))) throw  notEqualTheNumber(); // ����������: ������������ ����_1 - ����������� ������ ����� ������� �����.
                        else
                            for (unsigned int i = 1; i < buf.length(); i++)
                                if (!(('0' <= buf[i]) || (buf[i] <= '9'))) throw  notMatchTheNumbers(); // ����������: ������������ ����_3 - � ������ ������������ ��������, ����������������� ������.
                                else
                                    coeff = coeff * 10 + (buf[i] - 48);
                if (buf[NULL] == '-') coeff = coeff * k;
                break;
            case 1: // ��������� ������� � ������.
                if ((buf.length() > 1) || !(('0' <= buf[NULL]) && (buf[NULL] <= '9'))) throw  theExtentNotIncluded(); // ����������: ������������ ����_2 - ������� �� ������ � ���������� [0;9]
                else
                    index = index + (buf[NULL] - 48) * 100;
                break;
            case 2: // ��������� ������� y ������.
                if ((buf.length() > 1) || !(('0' <= buf[NULL]) && (buf[NULL] <= '9'))) throw  theExtentNotIncluded(); // ����������: ������������ ����_2 - ������� �� ������ � ���������� [0;9]
                else
                    index = index + (buf[NULL] - 48) * 10;
                break;
            case 3: // ��������� ������� z ������.
                if ((buf.length() > 1) || !(('0' <= buf[NULL]) && (buf[NULL] <= '9'))) throw  theExtentNotIncluded(); // ����������: ������������ ����_2 - ������� �� ������ � ���������� [0;9]
                else
                    index = index + (buf[NULL] - 48);
                break;
            }
        }
    }
    catch(pendingTheInput del){
        q.DelList();
        cout << del.what();
        cout << endl;
    }
     catch (notEqualTheNumber del) {
         q.DelList();
        cout << del.what();
        cout << endl;
    }
    catch (theExtentNotIncluded del) {
        q.DelList();
        cout << del.what();
        cout << endl;
    }
    catch (notMatchTheNumbers del) {
        q.DelList();
        cout << del.what();
        cout << endl;
    }
    return os;
}

ostream & operator<<(ostream & os, TPolinom & q) { // �����

    string p_out = "";
    string buf = "";
    string sign;

    int coef, ind, indx, indy, indz = NULL;

    for (q.Reset(); !q.IsListEnded(); q.GoNext()) {
        coef = q.getMonom()->GetCoeff();
        ind = q.getMonom()->GetIndex();
        indx = ind / 100;
        indy = (ind % 100) / 10;
        indz = ind % 10;

        sign = "+";
        if (coef < NULL)
            sign = "-";
        buf = to_string(abs(coef));

        if (indx)
            if (indx != 1)
                buf = buf + "x^" + to_string(indx);
            else
                buf = buf + "x";
        if (indy)
            if (indy != 1)
                buf = buf + "y^" + to_string(indy);
            else
                buf = buf + "y";
        if (indz)
            if (indz != 1)
                buf = buf + "z^" + to_string(indz);
            else
                buf = buf + "z";

        if (p_out != "")
            p_out = p_out + " " + sign + " " + buf;
        else {
            if (sign == "-")
                p_out += sign;
            p_out += buf;
        }
    }
    os << p_out << endl;;
    return os;
}

