#include "TPolynom.h"

TPolynom:: TPolynom(int monoms[][2], int km) {
	PTMonom Mon=new TMonom(0,-1);
	pHead->setDatValue(Mon);
	for (int i = 0; i < km; i++) {
		Mon = new TMonom(monoms[i][0], monoms[i][1]);
		insLast(Mon);
	}	
}

TPolynom:: TPolynom(TPolynom &q) {
	PTMonom Mon = new TMonom(0, -1);
	pHead->setDatValue(Mon);
	for (q.reset(); !q.isListEnded(); q.goNext()) {
		Mon = q.getMonom();
		insLast(Mon->getCopy());
	}
}

TPolynom TPolynom:: operator+(TPolynom &q) {
	TPolynom res;

	q.reset();
	reset();

	const TMonom nul(0, -1);
	TMonom mon;
	TMonom mon_q;				
	PTMonom tmp;
	
	while ( !isListEnded() || !q.isListEnded() ) {

		mon = isListEnded() ? nul: *getMonom();
		mon_q = q.isListEnded() ? nul: *q.getMonom();

		if (mon == nul) {
			tmp = new TMonom( mon_q.getCoeff(), mon_q.getIndex() );
			q.goNext();
		} else if (mon_q == nul) {
			tmp = new TMonom( mon.getCoeff(), mon.getIndex() );
			goNext();
		} else if (mon < mon_q) {
			tmp = new TMonom( mon.getCoeff(), mon.getIndex() );
			goNext();
		} else if (mon_q < mon) {
			tmp = new TMonom( mon_q.getCoeff(), mon_q.getIndex() );
			q.goNext();
		} else if ( mon.getIndex() == mon_q.getIndex() ) {
			tmp = new TMonom( mon.getCoeff() + mon_q.getCoeff(), mon.getIndex() );
			goNext();
			q.goNext();
		} else
			break;

		if ( tmp->getCoeff() )
			res.insLast(tmp);
	}
	return res;
}


TPolynom & TPolynom:: operator=(TPolynom &q) {
	delList();
	
	if ( (&q!=NULL) && (&q != this) ) {
		PTMonom Mon = new TMonom(0, -1);
		pHead->setDatValue(Mon);
		for (q.reset(); !q.isListEnded(); q.goNext()) {
			Mon = q.getMonom();
			insLast(Mon->getCopy());
		}
	}
	return *this;
}

TPolynom TPolynom:: derivative(Variables v) {
	TPolynom result;
	int Index;

	auto IndexVar = [v](unsigned Index) {
		unsigned result;
		switch (v) {
		case z: result = Index % 10; break;
		case y:  result = Index % 100 / 10; break;
		case x:  result = Index / 100; break;
		}
		return result;
		
	};
	
	PTMonom Mon = new TMonom(0, -1);
	for (reset(); !isListEnded(); goNext()) {
		Index = getMonom()->getIndex();
		if ( IndexVar( Index ) ) {
			Mon = new TMonom( getMonom()->getCoeff() * IndexVar( Index), Index-v );
			result.insLast(Mon);
		}
	}
	return result;
}

bool TPolynom:: operator==(TPolynom &q) {
	bool result = false;
	if (q.getListLength() == getListLength()) {
		result = true;
		for (q.reset(), reset(); !q.isListEnded(); q.goNext(), goNext())
			if (*(PTMonom)q.getDatValue() != *(PTMonom)getDatValue()) {
				result = false;
				break;
			}
	}
	return result;
}

ostream & operator<<(ostream & os, TPolynom & q) {
	auto CovertIndex = [](int index) {
		int tmp;
		string result;
		if (tmp = index / 100)
			result += tmp > 1 ? "x^" + lexical_cast <string> (tmp) : "x";
		if (tmp = index % 100 / 10)
			result += tmp > 1 ? "y^" + lexical_cast <string> (tmp) : "y";
		if (tmp = index % 10)
			result += tmp > 1 ? "z^" + lexical_cast <string> (tmp) : "z";
		return result;
	};

	q.reset();
	os << q.getMonom()->getCoeff() << CovertIndex(q.getMonom()->getIndex());
	q.goNext();
	for ( ; !q.isListEnded(); q.goNext() )
		os << ( q.getMonom()->getCoeff() > 0 ? " +" : " " ) << q.getMonom()->getCoeff() << CovertIndex(q.getMonom()->getIndex() );
	return os;
}


typedef regex_iterator<string::iterator> inter;

istream & operator>>(istream & os, TPolynom & q) {

	int i = 0, coeff,index;
	string buf;

	os >> buf;

	try {
		PTMonom mon;

		regex re_input("(([\\+|-]|^)[1-9](\\d+)?x\\^\\dy\\^\\dz\\^\\d)+");
		regex re_coeff("^\\d+|(\\+|-)((\\d+))");
		regex re_index("(\\^\\d)");

		if (!regex_match(buf, re_input)) throw exception ("Wrong input format");

		inter it_index(buf.begin(), buf.end(), re_index);
		inter it_coeff(buf.begin(), buf.end(), re_coeff);

		for (; it_coeff != inter(); ++it_coeff, ++i) {
			coeff = lexical_cast <int>(it_coeff->str());

			index = lexical_cast <int>(it_index++->str()[1]) * 100 + 
			lexical_cast <int>(it_index++->str()[1]) * 10 +       
			lexical_cast <int>(it_index++->str()[1]);             
			
			mon = new TMonom(coeff, index);
			q.insLast(mon);
		}

	}

	catch (const regex_error& e) {
		cout << e.what() << endl;
	}
	catch (const exception& e) {
		cout << e.what() << endl;
	}
	catch (...) {
		cout << "Unknown error" << endl;
	}
	return os;
}
