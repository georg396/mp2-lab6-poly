#include "THeadRing.h"

THeadRing:: THeadRing() :TDatList() {
	// create null link
	insLast();
	pHead = pFirst;
	pStop = pHead;
	reset();
	ListLen = 0;
	pFirst->setNextLink(pFirst);
}

THeadRing:: ~THeadRing() {
	delList();
	delLink(pHead);
	pHead = NULL;
}

void THeadRing:: insFirst(PTDatValue pVal) {
	TDatList:: insFirst(pVal);
	pHead->setNextLink(pFirst);
}

void THeadRing:: delFirst(void) {
	TDatList:: delFirst();
	pHead->setNextLink(pFirst);
}