#include "gtest/gtest.h"
#include "TPolynom.h"

TEST(TPolynom, Can_Create_Polynom_From_One_Monom) {
	// Arrange
	int mon[][2] = { {1, 3} };

	// Act
	TPolynom A(mon, 1);

	// Assert
	TMonom res(1, 3);
	EXPECT_EQ(res, *A.getMonom());
}

TEST(TPolynom, Can_Create_Polynom_From_Two_Monoms) {
	// Arrange
	const int size = 2;
	int mon[][2] = { {1, 3}, {2, 4} };

	// Act
	TPolynom Pol (mon, size);

	// Assert
	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.goNext())
		EXPECT_EQ(monoms[i], *Pol.getMonom());
}

TEST(TPolynom, Can_Create_Polynom_From_Ten_Monoms) {
	// Arrange
	const int size = 10;
	int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 }, { 3, 110 },
	{ 5, 150 }, { 6, 302 }, { 3, 400 }, { 2, 500 }, { 7 ,800 }, { 2, 888 } };
	
	// Act
	TPolynom Pol(mon, size);
	
	// Assert
	TMonom monoms[size];
	for (int i = 0; i < size; i++)
		monoms[i] = TMonom(mon[i][0], mon[i][1]);
	for (int i = 0; i < size; i++, Pol.goNext())
		EXPECT_EQ(monoms[i], *Pol.getMonom());
}

TEST(TPolynom, Can_Compare_The_Polynoms) {
	// Arrange
	const int size = 3;
	int mon[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 } };
	TPolynom Pol1(mon, size);
	TPolynom Pol2(mon, size);

	// Act & Assert
	EXPECT_TRUE(Pol1==Pol2);
}

TEST(TPolynom, Can_Copy_Polynoms) {
	// Arrange
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolynom Pol1(mon, size);

	// Act
	TPolynom Pol2 = Pol1;

	// Assert
	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolynom, Can_Assign_Polynoms) {
	// Arrange
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolynom Pol1(mon, size);
	TPolynom Pol2;

	// Act
	Pol2 = Pol1;

	// Assert
	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolynom, Can_Assign_Empty_Polynom) {
	// Arrange
	TPolynom Pol1;
	TPolynom Pol2;

	// Act
	Pol2 = Pol1;

	// Assert
	EXPECT_TRUE(Pol1 == Pol2);
}

TEST(TPolynom, Can_Add_Linear_Polynoms) {
	// Arrange
	const int size = 1;
	int mon1[][2] = { { 2, 1 } };
	int mon2[][2] = { { 1, 1 } };
	// 2z
	TPolynom Pol1(mon1, size);
	// z
	TPolynom Pol2(mon2, size);

	// Act
	TPolynom Pol = Pol1 + Pol2;

	// Assert
	const int expected_size = 1;
	int expected_mon[][2] = { { 3, 1 } };
	// z+2z^2
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolynom, Can_Add_Simple_Polynoms) {
	// Arrange
	const int size1 = 3;
	const int size2 = 4;
	int mon1[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	int mon2[][2] = { { 1, 1 }, { -8, 3 }, { 1, 4 }, { 2, 5 } };
	// 5z^2+8z^3+9z^4
	TPolynom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolynom Pol2(mon2, size2);

	// Act
	TPolynom Pol = Pol1 + Pol2;

	// Assert
	const int expected_size = 4;
	int expected_mon[][2] = { { 1, 1 }, { 5, 2 }, { 10, 4 }, { 2, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolynom, Can_Add_Polynoms) {
	// Arrange
	const int size1 = 5;
	const int size2 = 4;
	int mon1[][2] = { { 5, 213 }, { 8, 321 }, { 10, 432 }, { -21, 500 }, { 10, 999 } };
	int mon2[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	// 5x^2yz^3+8x^3y^2z+10x^4y^3z^2-21x^5+10x^9y^9z^9
	TPolynom Pol1(mon1, size1);
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolynom Pol2(mon2, size2);

	// Act
	TPolynom Pol = Pol1 + Pol2;

	// Assert
	const int expected_size = 6;
	int expected_mon[][2] = { { 15, 0 }, { 5, 213 }, { 10, 432 }, { -20, 500 }, { 20, 702 }, { 10, 999 } };
	// 15+5x^2yz^3+10x^4y^3z^2-20x^5+20x^7z^2+10x^9y^9z^9
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolynom, Can_Add_Many_Polynoms) {
	// Arrange
	const int size1 = 3;
	const int size2 = 4;
	const int size3 = 3;
	int mon1[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	int mon2[][2] = { { 1, 1 }, { -8, 3 }, { 1, 4 }, { 2, 5 } };
	int mon3[][2] = { { 10, 0 }, { 2, 3 }, { 8, 5 } };
	// 5z^2+8z^3+9z^4
	TPolynom Pol1(mon1, size1);
	// z-8z^3+z^4+2z^5
	TPolynom Pol2(mon2, size2);
	// 10+2z^3+8z^5
	TPolynom Pol3(mon3, size3);

	// Act
	TPolynom Pol = Pol1 + Pol2 + Pol3;

	// Assert
	const int expected_size = 6;
	int expected_mon[][2] = { { 10, 0 }, { 1, 1 }, { 5, 2 }, { 2, 3 }, { 10, 4 }, { 10, 5 } };
	// z+5z^2+10z^4+2z^5
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}

TEST(TPolynom, Can_Take_Derivative_Of_Simple_Polynoms) {
	// Arrange
	const int size = 3;
	int mon[][2] = { { 5, 2 }, { 8, 3 }, { 9, 4 } };
	// 5z^2+8z^3+9z^4
	TPolynom Pol(mon, size);

	// Act
	TPolynom derivative_Pol = Pol.derivative( z );

	// Assert
	const int expected_size = 3;
	int expected_mon[][2] = { { 10, 1 }, { 24, 2 }, { 36, 3 } };
	// 10z24z^2+36z^3
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolynom, Can_Take_Derivatives_Of_Complex_Polynoms_Over_The_Variable_x) {
	// Arrange
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolynom Pol(mon, size);

	// Act
	TPolynom derivative_Pol = Pol.derivative(x);

	// Assert
	const int expected_size = 3;
	int expected_mon[][2] = { { -24, 221 }, { 5, 400 }, { 140, 602 } };
	// -24x^2y^2z + 5x^4 + 140x^6z^2
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolynom, Can_Take_Derivatives_Of_Complex_Polynoms_Over_The_Variable_y) {
	// Arrange
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolynom Pol(mon, size);

	// Act
	TPolynom derivative_Pol = Pol.derivative(y);

	// Assert
	const int expected_size = 1;
	int expected_mon[][2] = { { -16, 311 } };
	// -16x^3yz
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}

TEST(TPolynom, Can_Take_Derivatives_Of_Complex_Polynoms_Over_The_Variable_z) {
	// Arrange
	const int size = 4;
	int mon[][2] = { { 15, 0 }, { -8, 321 }, { 1, 500 }, { 20, 702 } };
	// 15-8x^3y^2z+x^5+20x^7z^2
	TPolynom Pol(mon, size);

	// Act
	TPolynom derivative_Pol = Pol.derivative(z);

	// Assert
	const int expected_size = 2;
	int expected_mon[][2] = { { -8, 320 }, { 40, 701 } };
	// -8x^3y^2+7x^3z
	TPolynom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(derivative_Pol == expected_Pol);
}