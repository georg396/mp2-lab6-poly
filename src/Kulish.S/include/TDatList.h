#pragma once
#include "TDatLink.h"
// Lists
enum TLinkPos {FIRST,CURRENT,LAST};

class TDatList{
protected:
	PTDatLink pFirst;    // first element
	PTDatLink pLast;     // last element
	PTDatLink pCurrLink; // current element
	PTDatLink pPrevLink; // previous element
	PTDatLink pStop;     // end of list
	int CurrPos;         // nuber of current element (from 0)
	int ListLen;         // amount of elements in list
protected:
	PTDatLink getLink(PTDatValue pVal = NULL, PTDatLink pLink = NULL);
	void      delLink(PTDatLink pLink);   // delete element
public:
	TDatList();
	~TDatList() { delList(); }
	
	// access methods
	PTDatValue getDatValue(TLinkPos mode = CURRENT) const; // get element
	virtual int isEmpty()  const { return pFirst == pStop; } // is list empty?
	int getListLength()    const { return ListLen; }       // get amount of elements in list
	
	// navigation methods
	int setCurrentPos(int pos);          // set current list position
	int getCurrentPos(void) const;       // get number of current element
	virtual int reset(void);             // set to the start of the list
	virtual int isListEnded(void) const; // is list ended ?
	int goNext(void);                    // go to next element

	// methods of insertion elements
	virtual void insFirst(PTDatValue pVal = NULL); // insert to first position
	virtual void insLast(PTDatValue pVal = NULL); // insert to last position
	virtual void insCurrent(PTDatValue pVal = NULL); // insert to current position
	
	// methods of removing elements
	virtual void delFirst(void);      // delete first element
	virtual void delCurrent(void);    // delete current element
	virtual void delList(void);       // delete list
};
typedef TDatList *PTDataList;
